import{useState, useEffect, useContext} from 'react'
import{Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login(){

	// Allow us to consume the User Context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [password1, setPassword1] = useState('')
	const [email, setEmail] = useState('')


	// State to determine whether register button is enable or not
	const [isActive, setIsActive] = useState(true)

	console.log(email)
	console.log(password1)

	function authenticate(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// Set the email of the user in the local storage
		// Syntax:
			// localStorage.setItem('propertyName', value)
		localStorage.setItem('email', email);


		setUser({
			email:localStorage.getItem('email')
		})

		// Clears the input fields
		setEmail('');
		setPassword1('');
	
		alert('You are now logged in');
	}

	useEffect(() => {
		if((email !== '' && password1 !== '') && (password1 === password1)){

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [email, password1])

	return(
	(user.email !== null) ? 
		<Redirect to="/courses" />

		:

		<Form onSubmit={(e)=>authenticate(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type= 'email'
					placeholder= 'Enter email'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className = "text-muted">
					{/*We'll never share your email with anyone else.*/}
				</Form.Text>	
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type = 'password'
					placeholder = 'Password'
					value = {password1}
					onChange  = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 
				<Button variant = 'primary' type='submit' id='submitBtn' >
				Login
			</Button>

				:
				<Button variant = 'danger' type='submit' id = 'submitBtn' disabled>
					Login
				</Button>
			}
		</Form>
	)
}

/* Ternary Operator with the Button Tag
 ?- if -- <Button isActive, the color will be primary and it is enabled
 :- else -- elese<Button will be color danger and will be disabled> */

 /*useEffect-react  hooks
 Syntax: useEffect(() => {
 	<conditons>
 }, [parameter])

 --once a change happens in the parameters, 


 */