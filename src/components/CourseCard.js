import {useState} from 'react'

import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}) {

console.log(courseProp)

// object destructuring 
const {name, description, price} = courseProp
// Syntax: const {properties} = propname

// array destructuring
const [count, setCount] = useState(0)


// // // Syntax: const [getter, setter] =useState(initialValue)

// // // Hook used is useState- to store the state


// function enroll(){

//     setCount(count);
//     console.log('Enrollees' + count)

// }

const[seats, setSeats] = useState(30)

function enroll() {
   if(seats > 0){
   	setCount(count + 1);
   	console.log('Enrollees' + count)
   	setSeats(seats -1);
   	console.log('Seats' + seats)
   } else {
   		alert('No more seats available')
   }
}
   



    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Button variant="primary" onClick = {enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}